package com.itomych.nearbymessages

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.messages.Message
import com.google.android.gms.nearby.messages.MessageListener
import kotlin.random.Random

class NearbyBackgroundService(name: String) : IntentService(name) {

    private val CHANNEL_ID = "nearby_notify"
    private val CHANNEL_NAME = "nearby_notification_message"

    override fun onHandleIntent(intent: Intent?) {
        intent?.let {
            Nearby.getMessagesClient(this).handleIntent(intent, object : MessageListener() {
                override fun onFound(message: Message?) {
                    super.onFound(message)
                    message?.let {
                        showMessage(String(it.content))
                    }
                }

                override fun onLost(p0: Message?) {
                    super.onLost(p0)
                    deleteMessage()
                }
            })
        }
    }


    private fun showMessage(message: String) {
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("New message")
            .setContentText(message)
            .setAutoCancel(true)

        createNotificationChannel()

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(Random.nextInt(), notification.build())
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = CHANNEL_NAME
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun deleteMessage() {
        NotificationManagerCompat.from(this).cancelAll()
    }
}