package com.itomych.nearbymessages

import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.messages.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private val TAG = "NearbyMess"
    private val EXPIRE_TIME = 20 // sec

    private lateinit var messageListener: MessageListener
    private var message: Message? = null
    private val nearbyDevicesList = ArrayList<String>()
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, nearbyDevicesList)
        listView.adapter = adapter

        messageListener = object : MessageListener() {
            override fun onFound(mess: Message?) {
                super.onFound(mess)
                Log.d(TAG, "onFound")
                mess?.let {
                    Log.d(TAG, "message = $it")
                    adapter.add(AppMessage.fromNearbyMessage(it).userName)
                }
            }

            override fun onLost(mess: Message?) {
                super.onLost(mess)
                Log.d(TAG, "onLost")
                mess?.let {
                    val messText = AppMessage.fromNearbyMessage(it).userName
                    showToast("lost messText")
                    adapter.remove(messText)
                }
            }
        }

        sendButton.setOnClickListener {
            if (messageEditText.text.isNotEmpty()) {
                publish(messageEditText.text.toString())
            }
        }
    }

    override fun onStart() {
        super.onStart()

        publish("Alex  ${android.os.Build.MODEL}")
    }

    override fun onStop() {
        super.onStop()
        message?.let {
            Nearby.getMessagesClient(this).unpublish(it)
        }
        Nearby.getMessagesClient(this).unsubscribe(messageListener)
    }

    //send message and subscribe
    private fun publish(message: String) {

        val options = PublishOptions.Builder()
            .setStrategy(Strategy.Builder().setTtlSeconds(EXPIRE_TIME).build())
            .setCallback(object : PublishCallback() {
                override fun onExpired() {
                    super.onExpired()
                    runOnUiThread {
                        showToast("Publish expired")
                    }
                }
            }).build()

        this.message = AppMessage.newMessage(UUID.randomUUID().toString(), message).also {
            Nearby.getMessagesClient(this)
                .publish(it, options).addOnCompleteListener {
                    showToast("Publish successfully")
                }
            subscribe()
        }
    }

    private fun subscribe() {
        val options = SubscribeOptions.Builder()
            .setStrategy(Strategy.Builder().setTtlSeconds(EXPIRE_TIME + 10).build())
            .setCallback(object : SubscribeCallback() {
                override fun onExpired() {
                    super.onExpired()
                    runOnUiThread {
                        showToast("Subscribe expired")
                    }
                }
            }).build()

        Nearby.getMessagesClient(this).subscribe(messageListener, options).addOnCompleteListener {
            showToast("Subscribe successfully")
        }
    }

    private fun showToast(mess: String) {
        Toast.makeText(this@MainActivity, mess, Toast.LENGTH_LONG).show()
    }

    private fun getPendingIntent(): PendingIntent = PendingIntent.getBroadcast(
        this,
        0,
        Intent(this, NearbyBackgroundService::class.java),
        PendingIntent.FLAG_UPDATE_CURRENT
    )
}
